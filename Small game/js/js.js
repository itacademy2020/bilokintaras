let canvas, ctx, w, h, highCanvas; 
let mousePos;

// an empty array!
let balls = []; 
let lvlGame = 1;

let playerName = 'Player';
setTimeout(() => {
    let playerNameInput = document.querySelector('.Name');
    playerNameInput.addEventListener("change", (event) => {
        playerName = event.target.value;
    });
}, 100);


let SizePlayer = 20;
setTimeout(() => {
    let SizePlayerInput = document.querySelector('.SizePlayer');
    SizePlayerInput.addEventListener("change", (event) => {
        SizePlayer= event.target.value;
    });
}, 100);


let Speedball = 1;
setTimeout(() => {
    let SpeedballInput = document.querySelector('.Speedball');
    SpeedballInput.addEventListener("change", (event) => {
        Speedball = event.target.value;
    });
}, 100);


let Maxball = 30;
setTimeout(() => {
    let MaxballInput = document.querySelector('.Maxball');
    MaxballInput.addEventListener("change", (event) => {
        Maxball = event.target.value;
    });
}, 100);


let Minball = 5;
setTimeout(() => {
    let MinballInput = document.querySelector('.Minball');
    MinballInput.addEventListener("change", (event) => {
        Minball = event.target.value;
    });
}, 100);


let BallforWin = 10;
setTimeout(() => {
    let BallforWinInput = document.querySelector('.Errorball');
    BallforWin.addEventListener("change", (event) => {
        BallforWin = event.target.value;
    });
}, 100);

const player = {
    x: 10,
    y: 10,
    width: 25,
    height: 25,
  color: 'purple'
}

window.onload = function init() {
  // called AFTER the page has been loaded
    canvas = document.querySelector("#myCanvas");
    player.width = +SizePlayer;
    player.height = +SizePlayer;
  // often useful
  w = canvas.width; 
  h = canvas.height;  
  
  // important, we will draw with this object
    ctx = canvas.getContext('2d');
    high = canvas.getContext('2d');
  
  // create 10 balls
    balls = createBalls(BallforWin);
  
  // add a mousemove event listener to the canvas
  canvas.addEventListener('mousemove', mouseMoved);

  // ready to go !
    mainLoop();
    BallforWin++;
    lvlGame++;
};

function mouseMoved(evt) {
  mousePos = getMousePos(canvas, evt);
}

function getMousePos(canvas, evt) {
  // necessary work in the canvas coordinate system
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function movePlayerWithMouse() {
  if(mousePos !== undefined) {
    player.x = mousePos.x;
    player.y = mousePos.y;
  }
}

function mainLoop() {
  // 1 - clear the canvas
    ctx.clearRect(0, 0, w, h);
  // draw the ball and the player
  drawFilledRectangle(player);
  drawAllBalls(balls);
  drawNumberOfBallsAlive(balls);

  // animate the ball that is bouncing all over the walls
  moveAllBalls(balls);
  
  movePlayerWithMouse();
  
  // ask for a new animation frame
  requestAnimationFrame(mainLoop);
}

// Collisions between rectangle and circle
function circRectsOverlap(x0, y0, w0, h0, cx, cy, r) {
  let testX = cx;
  let testY = cy;
  if (testX < x0) testX = x0;
  if (testX > (x0 + w0)) testX = (x0 + w0);
  if (testY < y0) testY = y0;
  if (testY > (y0+h0)) testY = (y0 + h0);
  return (((cx - testX) * (cx - testX) + (cy - testY) * (cy - testY)) <  r * r);
}

function createBalls(n) {
  // empty array
  const ballArray = [];
  
  // create n balls
  for(let i=0; i < n; i++) {
    const b = {
      x: w/2,
      y: h/2,
      radius: (+Minball) + (+Maxball) * Math.random(), // between 5 and 35
      speedX: (-5 + 10 * Math.random()) * Speedball, // between -5 and + 5
      speedY: (-5 + 10 * Math.random()) * Speedball,//-5 + 10 * Math.random(), // between -5 and + 5
      color: getARandomColor(),
    }
    // add ball b to the array
     ballArray.push(b);
  }
  // returns the array full of randomly created balls
  return ballArray;
}

function getARandomColor() {
  const colors = ['red', 'blue', 'cyan', 'purple', 'pink', 'green', 'yellow'];
  // a value between 0 and color.length-1
  // Math.round = rounded value
  // Math.random() a value between 0 and 1
  let colorIndex = Math.round((colors.length-1) * Math.random()); 
  let c = colors[colorIndex];
  
  // return the random color
  return c;
}

function drawNumberOfBallsAlive(balls) {
  ctx.save();
  ctx.font="10px Arial";

    if (balls.length === +BallforWin) {
        ctx.fillText("YOU WIN!", 10, 30);
    }
    else {
        ctx.fillText(balls.length, 20, 30);
  }
  ctx.restore();
}

function drawAllBalls(ballArray) {
  ballArray.forEach(function(b) {
    drawFilledCircle(b);
  });
}

function moveAllBalls(ballArray) {
  // iterate on all balls in array
  ballArray.forEach(function(b, index) {
    // b is the current ball in the array
    b.x += b.speedX;
    b.y += b.speedY;
    testCollisionBallWithWalls(b); 
    testCollisionWithPlayer(b, index);
  });
}

function testCollisionWithPlayer(b, index) {
  if(circRectsOverlap(player.x, player.y,
                     player.width, player.height,
                     b.x, b.y, b.radius)) {
    // we remove the element located at index
    // from the balls array
    // splice: first parameter = starting index
    //         second parameter = number of elements to remove
    balls.splice(index, 1);
  }
}

function testCollisionBallWithWalls(b) {
  // COLLISION WITH VERTICAL WALLS ?
  if((b.x + b.radius) > w) {
    // the ball hit the right wall
    // change horizontal direction
    b.speedX = -b.speedX;
    
    // put the ball at the collision point
    b.x = w - b.radius;
  } else if((b.x -b.radius) < 0) {
    // the ball hit the left wall
    // change horizontal direction
    b.speedX = -b.speedX;
    
    // put the ball at the collision point
    b.x = b.radius;
  }
 
  // COLLISIONS WTH HORIZONTAL WALLS ?
  // Not in the else as the ball can touch both
  // vertical and horizontal walls in corners
  if((b.y + b.radius) > h) {
    // the ball hit the right wall
    // change horizontal direction
    b.speedY = -b.speedY;
    
    // put the ball at the collision point
    b.y = h - b.radius;
  } else if((b.y -b.radius) < 0) {
    // the ball hit the left wall
    // change horizontal direction
    b.speedY = -b.speedY;
    
    // put the ball at the collision point
    b.Y = b.radius;
  }  
}

function drawFilledRectangle(r) {
  // GOOD practice: save the context, use 2D trasnformations
  ctx.save();
  
  // translate the coordinate system, draw relative to it
  ctx.translate(r.x, r.y);
  
  ctx.fillStyle = r.color;
  // (0, 0) is the top left corner of the monster.
  ctx.fillRect(0, 0, r.width, r.height);
  
  // GOOD practice: restore the context
  ctx.restore();
}

function drawFilledCircle(c) {
  // GOOD practice: save the context, use 2D trasnformations
  ctx.save();
  
  // translate the coordinate system, draw relative to it
  ctx.translate(c.x, c.y);
  
  ctx.fillStyle = c.color;
  // (0, 0) is the top left corner
  ctx.beginPath();
  ctx.arc(0, 0, c.radius, 0, 2*Math.PI);
  ctx.fill();
 
  // GOOD practice: restore the context
  ctx.restore();
}

//let tbody = document.querySelector('tbody');